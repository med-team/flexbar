Source: flexbar
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Tony Travis <ajt@minke.ukfsn.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libtbb-dev,
               cmake,
               libseqan2-dev,
               zlib1g-dev,
               libbz2-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/flexbar
Vcs-Git: https://salsa.debian.org/med-team/flexbar.git
Homepage: https://github.com/seqan/flexbar
Rules-Requires-Root: no

Package: flexbar
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: flexible barcode and adapter removal for sequencing platforms
 Flexbar preprocesses high-throughput sequencing data efficiently. It
 demultiplexes barcoded runs and removes adapter sequences. Moreover,
 trimming and filtering features are provided. Flexbar increases mapping
 rates and improves genome and transcriptome assemblies. It supports
 next-generation sequencing data in fasta/q and csfasta/q format from
 Illumina, Roche 454, and the SOLiD platform.
 .
 Parameter names changed in Flexbar. Please review scripts. The recent
 months, default settings were optimised, several bugs were fixed and
 various improvements were made, e.g. revamped command-line interface,
 new trimming modes as well as lower time and memory requirements.
